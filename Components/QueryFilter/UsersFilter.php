<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.01.2019
 * Time: 18:04
 */

namespace App\Components\QueryFilter;

use Illuminate\Database\Eloquent\Builder;

class UsersFilter extends QueryFilter
{
    public function apply($builder): Builder
    {
        $builder = $builder->select('users.*');

        return parent::apply($builder);
    }

    public function name(string $value): Builder
    {
        return $this->builder->where('users.name', 'like', "$value%");
    }

    public function email(string $value): Builder
    {
        return $this->builder->where('users.email', 'like', "$value%");
    }

    public function created_at(array $date): Builder
    {
        [$dateFrom, $dateTo] = $date;
        if ($dateFrom && $dateTo) {
            return $this->builder->where('users.created_at', '>=', date('Y-m-d H:i:s', strtotime($dateFrom)))
                                 ->where('users.created_at', '<=', date('Y-m-d 23:59:59', strtotime($dateTo)));
        }

        if ($dateFrom && !$dateTo) {
            return $this->builder->where('users.created_at', '>=', date('Y-m-d H:i:s', strtotime($dateFrom)));
        }

        if (!$dateFrom && $dateTo) {
            return $this->builder->where('users.created_at', '<=', date('Y-m-d 23:59:59', strtotime($dateTo)));
        }

        return $this->builder;
    }

    public function role_id(int $value): Builder
    {
        return $this->builder->whereHas(
            'roles',
            static function ($query) use ($value) {
                $query->where('roles.id', '=', $value);
            }
        );
    }

    public function status_id(string $value): Builder
    {
        if(empty($value)){
            return $this->builder;
        }

        return $this->builder->where('users.status_id', $value);
    }

    public function parent_id(int $value): Builder
    {
        if ($value) {
            return $this->builder->whereHas(
                'parents',
                static function ($query) use ($value) {
                    $query->where('user_parent_child.parent_id', '=', $value);
                }
            );
        }

        return $this->builder->leftJoin(
            'user_parent_child',
            static function ($join) {
                $join->on('users.id', '=', 'user_parent_child.child_id');
            }
        )->whereNull('user_parent_child.parent_id');
    }
}
