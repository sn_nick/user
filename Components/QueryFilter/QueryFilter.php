<?php

/**
 * Created by PhpStorm.
 * User: Sekretarev Nickolay
 * Date: 30.09.2019
 * Time: 18:00
 */

namespace App\Components\QueryFilter;

use App\Models\User;
use Illuminate\Database\Eloquent\Builder;

use function method_exists;
use function strstr;

abstract class QueryFilter implements QueryFilterContract
{
    public const TABLE_ROLES = 'roles';
    public const TABLE_USERS = 'users';
    public const TABLE_PAYMENTS = 'payments';
    public const TABLE_BILLING_TRANSACTIONS = 'billing_transactions';
    public const TABLE_ACCOUNTS = 'accounts';
    public const TABLE_ACTION_LOG = 'action_log';

    /**
     * @var \Illuminate\Database\Eloquent\Builder $builder
     */
    protected $builder;
    protected $params;

    public function __construct(FilterParamsContract $params)
    {
        $this->params = $params;
    }

    public function apply($builder)
    {
        $this->builder = $builder;
        $this->applyFilters();
        $this->applySort();

        return $this->builder;
    }

    protected function applyFilters(): void
    {
        $filters = $this->params->filter();

        if (!empty($filters)) {
            try {
                foreach ($filters as $filter => $value) {
                    $methodName = str_replace('.', '__', $filter);
                    if (method_exists($this, $methodName)) {
                        $this->$methodName($value);
                        continue;
                    }
                    $this->builder->where($filter, $value);
                }
            } catch (\Throwable $exception) {
                throw new \LogicException($exception->getMessage());
            }
        }
    }

    protected function applySort(): void
    {
        $columns = \explode('|', $this->params->sort());
        if (empty($columns)) {
            $this->getSort('', 'id', 'desc');

            return;
        }

        foreach ($columns as $column) {
            if (empty($column)) {
                continue;
            }
            $order = \substr($column, -1) !== '-' ? self::SORT_ASC : self::SORT_DESC;
            $column = rtrim($column, '-');
            if (empty($column)) {
                continue;
            }
            $table = strstr($column, '.', true);
            $this->getSort($table ?: '', $column, $order);
        }
    }
}
