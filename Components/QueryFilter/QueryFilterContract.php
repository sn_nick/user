<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 30.01.2019
 * Time: 18:05
 */

namespace App\Components\QueryFilter;


interface QueryFilterContract
{
    /** @var string  */
    public const SORT_ASC = 'asc';
    /** @var string  */
    public const SORT_DESC = 'desc';

    public function apply($builder);
}
