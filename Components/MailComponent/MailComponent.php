<?php

namespace App\Components\MailComponent;

use App\Exceptions\CustomException;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Mail;
use App\Mail\{UserActiveStatus,
    UserAdd,
    UserBlockStatus,
    UserChannelConfirmStatus,
    UserChannelDenyStatus,
    UserEditPaymentRequisites,
    UserEditRequisites,
    UserRegistered,
    UserResetPassword,
    UserUpdatePassword,
    UserUpdateToken,
};

use App\Models\User;

class MailComponent implements MailComponentContract
{
    private static \Swift_Plugins_Logger $logger;
    protected string                     $scope;
    protected User                       $user;
    protected ?string                    $param;

    /**
     * @var array|string[]
     */
    public static array $availableMails = [
        self::MAIL_USER_ADD                     => UserAdd::class,
        self::MAIL_USER_UPDATE_PASSWORD         => UserUpdatePassword::class,
        self::MAIL_USER_REGISTERED              => UserRegistered::class,
        self::MAIL_USER_RESET_PASSWORD          => UserResetPassword::class,
        self::MAIL_USER_UPDATE_TOKEN            => UserUpdateToken::class,
        self::MAIL_USER_EDIT_PAYMENT_REQUISITES => UserEditPaymentRequisites::class,
        self::MAIL_USER_EDIT_REQUISITES         => UserEditRequisites::class,
        self::MAIL_USER_BLOCK_STATUS            => UserBlockStatus::class,
        self::MAIL_USER_ACTIVE_STATUS           => UserActiveStatus::class,
        self::MAIL_USER_CHANNEL_DENY_STATUS     => UserChannelDenyStatus::class,
        self::MAIL_USER_CHANNEL_CONFIRM_STATUS  => UserChannelConfirmStatus::class,
    ];

    /**
     * SendMail constructor.
     * @param string $scope
     * @param User $user
     * @param string|null $param
     */
    public function __construct(string $scope, User $user, string $param = null)
    {
        if (!isset(static::$logger)) {
            static::$logger = new \Swift_Plugins_Loggers_ArrayLogger();
            app('mailer')
                ->getSwiftMailer()
                ->registerPlugin(new \Swift_Plugins_LoggerPlugin(static::$logger));
        }
        $this->scope = $scope;
        $this->user = $user;
        $this->param = $param;
    }

    /**
     * @return void
     */
    public function send(): void
    {
        try{
            Mail::to($this->user)
                ->send(
                    ($this->getMail())->onQueue(config('constants.queue_send_mail'))
                );
        } catch (\Throwable $e){
            \Sentry\captureMessage(self::$logger->dump());
        }
    }

    private function getMail()
    {
        if (!class_exists(self::$availableMails[$this->scope])) {
            throw new CustomException(Response::HTTP_CONFLICT, __('validation.custom.error.action'));
        }

        if ($this->param === null) {
            return new self::$availableMails[$this->scope]($this->user);
        }

        return new self::$availableMails[$this->scope]($this->user, $this->param);
    }
}