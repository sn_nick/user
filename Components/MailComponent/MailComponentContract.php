<?php

namespace App\Components\MailComponent;

interface MailComponentContract
{
    public const MAIL_USER_ADD = 'user_add';
    public const MAIL_USER_UPDATE_PASSWORD = 'user_update_password';
    public const MAIL_USER_REGISTERED = 'user_registered';
    public const MAIL_USER_RESET_PASSWORD = 'user_reset_password';
    public const MAIL_USER_UPDATE_TOKEN = 'user_update_token';
    public const MAIL_USER_EDIT_PAYMENT_REQUISITES = 'user_edit_payment_requisites';
    public const MAIL_USER_EDIT_REQUISITES = 'user_edit_requisites';
    public const MAIL_USER_BLOCK_STATUS = 'user_block_status';
    public const MAIL_USER_ACTIVE_STATUS = 'user_active_status';
    public const MAIL_USER_CHANNEL_DENY_STATUS = 'user_channel_deny_status';
    public const MAIL_USER_CHANNEL_CONFIRM_STATUS = 'user_channel_confirm_status';

    public function send(): void;
}