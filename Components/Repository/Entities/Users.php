<?php
/**
 * Created by PhpStorm.
 * User: Sekretarev Nickolay
 * Date: 30.01.2019
 * Time: 18:04
 */

namespace App\Components\Repository\Entities;

use App\Components\Activity\Entity\UserAttribute;
use App\Components\Activity\LoggerActivity;
use App\Components\MailComponent\MailComponent;
use App\Exceptions\CustomException;
use App\Models\Payment;
use App\Models\Status;
use Illuminate\Http\Response;
use Throwable;
use App\Components\EntityPermissions\Users\{UserPermissionsBatch,
    UserPermissionsDestroy,
    UserPermissionsRestore,
    UserPermissionsShow,
    UserPermissionsStore,
    UserPermissionsUpdate
};
use App\Components\Repository\{DeleteBatchContract,
    IndexContract,
    RestoreBatchContract,
    ShowContract,
    StoreContract,
    UpdateContract
};
use App\Components\UserActions\{ActionContract, ActionService};
use App\Constants\{StatusCodes, Time};
use App\Helpers\Helper;
use App\Models\Account;
use App\Components\QueryFilter\FilterParams;
use App\Components\QueryFilter\UsersFilter;
use App\Components\Repository\DataChangeParamsContract;
use App\Components\Repository\DataRequestParamsContract;
use App\Models\Browser;
use App\Models\Device;
use App\Models\Permission;
use App\Models\Profile;
use App\Models\Statistic;
use App\Models\System;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use App\Components\MailComponent\MailComponentContract;
use function Sentry\captureException;
use function Sentry\captureMessage;

class Users extends Entity implements IndexContract, StoreContract, ShowContract, UpdateContract, DeleteBatchContract,
    RestoreBatchContract
{
    /**
     * @param DataRequestParamsContract $params
     * @return Collection
     */
    public function index(DataRequestParamsContract $params): Collection
    {
        $userPermissions = new UserPermissionsShow();

        if (!$userPermissions->can(Permission::MENU_USERS)) {
            throw new CustomException(StatusCodes::HTTP_FORBIDDEN, __('validation.custom.permission.denied'));
        }

        $auth = $userPermissions->getAuthByPermissions(
            [
                Permission::SHOW_ALL_USER,
                Permission::SHOW_CHILD_USER,
            ]
        );

        $filters = new UsersFilter(
            new FilterParams(
                [
                    'filter' => $params->getFilters(),
                    'sort'   => $params->getSort(),
                ]
            )
        );

        return $auth->filter($filters)->customPaginate($params->getLimit())->getCollection();
    }

    /**
     * @param DataRequestParamsContract $params
     * @return Model
     * @throws Throwable
     */
    public function store(DataRequestParamsContract $params): Model
    {
        $userPermissions = new UserPermissionsStore();
        $auth = $userPermissions->getAuthByPermissions(
            [
                Permission::STORE_ALL_USER,
                Permission::STORE_CHILD_USER,
            ]
        );

        $input = $params->getRequestParams();

        $password = Str::random(10);
        $data = [
            'password'  => \bcrypt($password),
            'url_token' => null,
            'verified'  => 1,
        ];

        $input = \array_merge($input, $data);

        DB::beginTransaction();
        try {
            $user = User::create($input);
            $user->assignRole($input['role']);

            // because laravel does not have event on attach()
            $activity = new LoggerActivity($user, (new UserAttribute())->rolesAttached([$input['role']]));
            $activity->save();

            $accounts = \get_accounts($user->id);
            Account::insert($accounts);
            Profile::firstOrCreate(['user_id' => $user->id]);

            if ($auth instanceof BelongsToMany) {
                $auth->attach($user->id, ['relation' => User::RELATION_MANAGER_USER]);

                // because laravel does not have event on attach()
                $activity = new LoggerActivity($auth->getParent(), (new UserAttribute())->childrenAttached([$user]));
                $activity->save();
            }
            DB::commit();
        } catch (ConflictHttpException $e) {
            DB::rollback();
            throw new CustomException(Response::HTTP_CONFLICT, __('validation.custom.error.update'));
        }

        $mail = new MailComponent(MailComponentContract::MAIL_USER_ADD, $user, $password);
        $mail->send();

        return $user;
    }

    /**
     * @param DataRequestParamsContract $params
     * @return Model
     */
    public function show(DataRequestParamsContract $params): Model
    {
        $userPermissions = new UserPermissionsShow();
        $auth = $userPermissions->getAuthByPermissions(
            [
                Permission::SHOW_ALL_USER,
                Permission::SHOW_CHILD_USER,
            ]
        );

        $user = $auth->where('users.id', $params->getRouteParam())
                     ->withTrashed()
                     ->first();

        if ($user === null) {
            throw new CustomException(Response::HTTP_NOT_FOUND, __('validation.custom.user.not_found'));
        }

        return $user;
    }

    /**
     * @param DataRequestParamsContract $params
     * @return Model
     * @throws Throwable
     */
    public function update(DataRequestParamsContract $params): Model
    {
        $input = $params->getRequestParams();

        $userPermissions = new UserPermissionsUpdate($input);
        $auth = $userPermissions->getAuthByPermissions(
            [
                Permission::USER_STATUS_UPDATE,
                Permission::USER_PASSWORD_UPDATE,
                Permission::USER_ROLE_UPDATE,
                Permission::USER_EMAIL_UPDATE,
                Permission::UPDATE_ALL_USER,
                Permission::UPDATE_CHILD_USER,
            ]
        );

        $this->getParamsByAuth($input, $auth);

        $user = $auth->where('users.id', $params->getRouteParam())
                     ->withTrashed()
                     ->first();

        if ($user === null) {
            throw new CustomException(Response::HTTP_NOT_FOUND, __('validation.custom.user.not_found'));
        }

        /** @var User $user */

        \DB::beginTransaction();
        try {
            $user->updateRolesWithActivity($input);
            $user->update($input);
        } catch (ConflictHttpException $exception) {
            \DB::rollBack();
            throw new CustomException(Response::HTTP_CONFLICT, __('validation.custom.error.update'));
        }
        \DB::commit();

        return $user;
    }

    /**
     * @param DataRequestParamsContract $params
     * @return array
     * @throws Throwable
     */
    public function destroy(DataRequestParamsContract $params): array
    {
        $userPermissions = new UserPermissionsDestroy();
        $auth = $userPermissions->getAuthByPermissions(
            [
                Permission::DESTROY_ALL_USER,
                Permission::DESTROY_CHILD_USER,
            ]
        );

        $ids = $params->getRequestParams()['user_ids'] ?? [];

        if (\in_array(User::LAND_MASTER_ID, $ids, true)) {
            throw new CustomException(Response::HTTP_CONFLICT, __('validation.custom.error.delete'));
        }

        $user = $auth->whereIn('users.id', $ids)
                     ->withTrashed();

        if ($user->first() === null) {
            throw new CustomException(Response::HTTP_NOT_FOUND, __('validation.custom.user.not_found'));
        }

        try {
            $user->update(
                [
                    'deleted_at' => \now(),
                    'status_id'  => Status::STATUS_DELETED_ID,
                ]
            );
        } catch (ConflictHttpException $exception) {
            throw new CustomException(Response::HTTP_CONFLICT, __('validation.custom.error.delete'));
        }

        return [];
    }

    /**
     * @param DataRequestParamsContract $params
     * @return array
     * @throws Throwable
     */
    public function restore(DataRequestParamsContract $params): array
    {
        $userPermissions = new UserPermissionsRestore();
        $auth = $userPermissions->getAuthByPermissions(
            [
                Permission::RESTORE_ALL_USER,
                Permission::RESTORE_CHILD_USER,
            ]
        );

        $ids = $params->getRequestParams()['user_ids'] ?? [];

        $user = $auth->whereIn('users.id', $ids)
                     ->onlyTrashed();

        if ($user->first() === null) {
            throw new CustomException(Response::HTTP_NOT_FOUND, __('validation.custom.user.not_found'));
        }

        try {
            $user->withTrashed()
                 ->update(
                     [
                         'deleted_at' => null,
                         'status_id'  => Status::STATUS_ACTIVE_ID,
                     ]
                 );
        } catch (ConflictHttpException $exception) {
            throw new CustomException(Response::HTTP_CONFLICT, __('validation.custom.error.restore'));
        }

        return [];
    }
}
