<?php

namespace App\Http\Controllers\API;

use App\Components\Repository\Entities\Users;
use App\Http\Requests\User\{UserActionRequest,
    UserApiTokenUpdateRequest,
    UserBatchRequest,
    UserFilterRequest,
    UserRestoreRequest,
    UserShowRequest,
    UserStatisticsRequest,
    UserStoreRequest,
    UserDeleteRequest,
    UserUpdateRequest
};
use App\Http\Resources\Users\{UsersForSelectResourceCollection,
    UsersResource,
    UsersResourceCollection
};
use Throwable;

class UserController extends BaseController
{
    /**
     * @param UserFilterRequest $request
     * @return UsersResourceCollection
     */
    public function index(UserFilterRequest $request): UsersResourceCollection
    {
        $repository = Users::repository();
        $data = $repository->index($request);
        $meta = $repository->addPagination()->addDictionaries($request)->getMeta();

        $resource = new UsersResourceCollection($data);

        return $resource->additional(['meta' => $meta]);
    }

    /**
     * @param UserStoreRequest $request
     * @return UsersResource
     * @throws Throwable
     */
    public function store(UserStoreRequest $request): UsersResource
    {
        $repository = Users::repository();
        $data = $repository->store($request);

        return new UsersResource($data);
    }

    /**
     * @param UserShowRequest $request
     * @return UsersResource
     */
    public function show(UserShowRequest $request): UsersResource
    {
        $repository = Users::repository();
        $data = $repository->show($request);
        $meta = $repository->addDictionaries($request)->getMeta();

        $resource = new UsersResource($data);

        return $resource->additional(['meta' => $meta]);
    }

    /**
     * @param UserUpdateRequest $request
     * @return UsersResource
     * @throws Throwable
     */
    public function update(UserUpdateRequest $request): UsersResource
    {
        $repository = Users::repository();
        $data = $repository->update($request);

        return new UsersResource($data);
    }

    /**
     * @param UserDeleteRequest $request
     * @return UsersResource
     */
    public function destroy(UserDeleteRequest $request): UsersResource
    {
        $repository = Users::repository();
        $data = $repository->destroy($request);

        return new UsersResource($data);
    }

    /**
     * @param UserRestoreRequest $request
     * @return UsersResource
     */
    public function restore(UserRestoreRequest $request): UsersResource
    {
        $repository = Users::repository();
        $data = $repository->restore($request);

        return new UsersResource($data);
    }
}
