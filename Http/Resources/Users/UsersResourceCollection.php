<?php

namespace App\Http\Resources\Users;

use App\Http\Resources\ResourceWith;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;

class UsersResourceCollection extends ResourceCollection
{
    use ResourceWith;
    use UsersData;

    /**
     * Transform the resource collection into an array.
     *
     * @param Request $request
     *
     * @return Collection
     */
    public function toArray($request): Collection
    {
        return $this->collection;
    }
}
