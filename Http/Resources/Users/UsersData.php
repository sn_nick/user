<?php

namespace App\Http\Resources\Users;

use App\Models\User;

trait UsersData
{
    /**
     * @param $user
     *
     * @return array
     */
    private function getData($user): array
    {
        /** @var User $user */

        if (empty($user->resource)) {
            return [];
        }

        return [
            'id'              => $user->id,
            'name'            => $user->name,
            'email'           => $user->email,
            'contacts'        => $user->contacts,
            'created_at'      => $user->created_at ? $user->created_at->format('Y-m-d H:i:s') : null,
            'updated_at'      => $user->updated_at ? $user->updated_at->format('Y-m-d H:i:s') : null,
            'deleted_at'      => $user->deleted_at ? $user->deleted_at->format('Y-m-d H:i:s') : null,
            'settings'        => $user->settings,
            'params'          => $user->params,
            'auto_moderation' => (int)$user->auto_moderation,
            'status_id'       => (int)$user->status_id,
            'roles'           => $user->roles()->get(),
            'children'        => $user->children()->get(),
            'parents'         => $user->parents()->get(),
        ];
    }
}