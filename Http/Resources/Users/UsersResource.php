<?php

namespace App\Http\Resources\Users;

use App\Http\Resources\ResourceWith;
use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class UsersResource extends JsonResource
{
    use ResourceWith;
    use UsersData;

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     *
     * @return array
     */
    public function toArray($request): array
    {
        return $this->getData($this);
    }
}
