<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;
use App\Models\Permission;
use App\Models\Role;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;

class UserUpdateRequest extends BaseRequest
{
    protected array $allowedParams = ['name', 'email', 'contacts', 'password', 'role', 'status_id',];

    public function rules(): array
    {
        $id = $this->route('id');

        return [
            'name'      => 'required|string|max:40',
            'email'     => 'nullable|email|max:255|unique:users,email,' . $id,
            'contacts'  => 'string|nullable',
            'role'      => 'nullable|exists:roles,name',
            'password'  => 'nullable|string',
            'status_id' => 'nullable|in:4,5',
        ];
    }

    public function routeParamsErrors()
    {
        $validator = Validator::make(
            $this->route()->parameters(),
            [
                'id' => 'integer',
            ]
        );

        if ($validator->fails()) {
            return $validator->errors();
        }

        return false;
    }
}
