<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\File;

class UserFilterRequest extends BaseRequest
{
    protected array $allowedParams = ['page', 'limit', 'sort', 'fields', 'filters'];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'page'    => 'integer',
            'limit'   => 'integer',
            'sort'    => ['string', 'regex:/^(id|name|email|created_at|roles.name)(\+|-|)$/u'],
            'fields'  => 'string',
            'filters' => 'json',
        ];
    }

    public function schemaErrors()
    {
        //Валидация json schema
        $data = $this->all();
        if (isset($data['filters'])) {
            $data['filters'] = \json_decode($data['filters']);
        }

        return $this->schemaValidate(File::get(resource_path('json-schema/users/request-filter.json')), $data);
    }
}
