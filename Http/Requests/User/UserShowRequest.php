<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\Validator;

class UserShowRequest extends BaseRequest
{
    public function routeParamsErrors()
    {
        $validator = Validator::make(
            $this->route()->parameters(),
            [
                'id' => 'numeric',
            ]
        );

        if ($validator->fails()) {
            return $validator->errors();
        }

        return false;
    }
}
