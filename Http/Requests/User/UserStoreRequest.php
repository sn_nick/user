<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;
use App\Models\Role;

class UserStoreRequest extends BaseRequest
{
    protected array $allowedParams = ['name', 'email', 'contacts', 'role', 'user_type'];

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'name'      => 'required|string|max:40',
            'email'     => 'required|email|max:255|unique:users',
            'contacts'  => 'string|nullable',
            'role'      => 'nullable|string|exists:roles,name',
        ];
    }
}
