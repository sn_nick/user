<?php

namespace App\Http\Requests\User;

use App\Http\Requests\BaseRequest;
use Illuminate\Support\Facades\Auth;

class UserRestoreRequest extends BaseRequest
{
    public function rules(): array
    {
        return [
            'user_ids'   => 'array',
            'user_ids.*' => 'required|exists:users,id,id,!' . Auth::id(),
        ];
    }
}
